Para correr el proyecto localmente:

1. Clonar el repositorio
2. Correr docker-compose up --build para subir los contenedores localmente si se quiere
3. Puede que haya un error del entrypoint de la creación de la base de datos. (Hay que crear la base de datos manualmente):
   - docker exec -it SQLContainerID /bin/bash
   - /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P myPassw0rd -d master -i /docker-entrypoint-initdb.d/init.sql
4. Repetir docker-compose up web (para correr el contenedor de la aplicación web flask localmente)



Que se hiso:

- Se creó la app de Flask. Utiliza SQAlchemy para la conexión a la base de datos.
- Se creó el modelo de la base de datos.
- Se creó 2 puntos de entrada /productos y /productos/<id>
- Se llenó desde Flask la base de datos con productos de prueba.
- Se creó el Dockerfile para la aplicación web.
- Se creó el docker-compose.yml para levantar la aplicación web y la base de datos.
- Se creó un archivo init.sql para crear la base de datos y la tabla de productos y se lo agregó en un volumen.
- Se creó un archivo .env para las variables de entorno.
- Se creó un repositorio en Gitlab y se subió el código.
- Se creó una pipeline para Gitlab que hace el build de la imagen y envía la imagen al registry de Azure.
- Se configuró variables de entorno seguras en Gitlab y para docker-compose.
- Se creó un container registry en Azure.
- Se creó una base de datos en Azure.
- Se estableció conexión con la base de datos desde la aplicación Flask.
- Se creó la tabla de productos en la base de datos de Azure desde la aplicación Flask y se introdujo datos. 
- Se utilizó Terraform para crear un Azure Container Instance y un Resource Group. 
- Se configuró variables de entorno seguras en Terraform.

Que falta hacer:
- debuggear el Public IP del Azure Container Instance. (No se puede acceder desde el navegador)
- comentar el código y agregar docstrings.
- agregar tests.


Nota:

La verdad me gustó mucho el reto. Me gustaría seguir aprendiendo más sobre Azure y Terraform. El error en Azure
lo he visto en AWS y sé cómo solucionarlo allá, pero ya no me dio el tiempo para descubrir cómo solucionarlo en Azure.
Es la primera vez que uso Terraform y me resultó más sencillo de lo que pensaba. 
La conexión a la base de datos desde Flask dio muchos problemas por cuestiones de versiones de librerías y 
de la imagen de Docker, aunque al final se pudo solucionar, con django y postgres (que es lo que he usado mucho) 
quizás sí hubiera tenido el tiempo para hacerlo todo.




